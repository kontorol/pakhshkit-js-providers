//@flow
import KontorolPlaybackSource from './kontorol-playback-source';

export default class KontorolBumper {
  /**
   * @member - The bumper entry ID
   * @type {string}
   */
  entryId: string;
  /**
   * @member - The bumper click through url
   * @type {string}
   */
  clickThroughUrl: string;
  /**
   * @member - The bumper sources
   * @type {Array<KontorolPlaybackSource>}
   */
  sources: Array<KontorolPlaybackSource>;

  constructor(data: any) {
    this.entryId = data.entryId;
    this.clickThroughUrl = data.url;
    this.sources = data.sources ? data.sources.map(source => new KontorolPlaybackSource(source)) : [];
  }
}
